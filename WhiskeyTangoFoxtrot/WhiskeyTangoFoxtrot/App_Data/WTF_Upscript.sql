﻿CREATE TABLE [dbo].[WTFUsers] (
	
	[WTFUserID]			INT IDENTITY (1,1)	NOT NULL,
	[Username]			NVARCHAR(64)		NULL,
	[LoginCount]		INT					DEFAULT 1,
	[AspNetIdentityID]	NVARCHAR(128)		NOT NULL,

	CONSTRAINT [PK_dbo.WTFUsers] PRIMARY KEY CLUSTERED ([WTFUserID] ASC)
);

CREATE TABLE [dbo].[UserProfile] (
	[WTFUserID]					INT					NOT NULL,
	[FirstName]					NVARCHAR(64)		NOT NULL,
	[LastName]					NVARCHAR(64)		NOT NULL,
	[UserCity]					NVARCHAR(64)		NULL,
	[Bio]						NVARCHAR (max)		NULL,
	[ProfileIsPrivate]			Bit					NOT NULL,

	CONSTRAINT [PK_dbo.UserProfile.WTFUserID] PRIMARY KEY CLUSTERED	([WTFUserID] ASC),

	CONSTRAINT [FK_dbo.UserProfile.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[Forum] (
	[WTFUserID]		INT				NOT NULL,
	[ForumName]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(256)	NOT NULL,
	[IsActive]		Bit				NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.Forum.WTFUserID] PRIMARY KEY CLUSTERED ([WTFUserID] ASC),

	CONSTRAINT [FK_dbo.Forum.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[ChatBox] (
	[ChatBoxID]		INT				NOT NULL,
	[ChatName]		NVARCHAR(64)	NOT NULL,
	[ChatTopic]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(256)	NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.ChatBox.ChatBoxID] PRIMARY KEY CLUSTERED ([ChatBoxID] ASC),
);

CREATE TABLE [dbo].[ChatMessage] (
	[MessageID]			INT			NOT NULL,	
	[WTFUserID]			INT			NOT NULL,
	[ChatBoxID]			INT			NOT NULL,
	[MessageDate]		DateTime	NOT NULL,
	[MessageContent]	NVARCHAR	NOT NULL,
	[ReplyID]			INT			NULL

	CONSTRAINT [PK_dbo.ChatMessage.MessageID] PRIMARY KEY CLUSTERED ([MessageID] ASC),

	CONSTRAINT [FK_dbo.ChatMessage.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.ChatMessage.ChatBoxID] 
	FOREIGN KEY ([ChatBoxID]) REFERENCES [dbo].[ChatBox] ([ChatBoxID]),
);