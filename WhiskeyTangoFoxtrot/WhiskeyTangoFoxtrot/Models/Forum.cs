namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Forum")]
    public partial class Forum
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WTFUserID { get; set; }

        [Required]
        [StringLength(64)]
        public string ForumName { get; set; }

        [Required]
        [StringLength(256)]
        public string Message { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsMemberOnly { get; set; }

        public virtual WTFUser WTFUser { get; set; }
    }
}
