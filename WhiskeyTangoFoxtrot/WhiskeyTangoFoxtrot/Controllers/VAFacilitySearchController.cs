﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiskeyTangoFoxtrot.Controllers
{
    public class VAFacilitySearchController : Controller
    {
        readonly string key = System.Web.Configuration.WebConfigurationManager.AppSettings["apiKey"];

        [HttpGet]
        public JsonResult VASearch(string qurl)
        {
            //var client = new RestClient
            //{
            //    BaseUrl = new Uri("https://dev-api.va.gov/services/va_facilities/v0/")
            //};

            //var request = new RestRequest();
            //request.AddHeader("apiKey", key);

            //IRestResponse response = client.Execute(request);

            //return Json(response);

            string baseUrl = "https://dev-api.va.gov/services/va_facilities/v0/";
            string url = baseUrl + qurl;

            var client = new RestClient(url);

            var request = new RestRequest(Method.GET);
            request.AddHeader("apiKey", key);

            var response = client.Execute(request);
            return Json(response);
        }
    }
}