﻿$(document).ready(function () {

    $('searchButton').click(function () {

        var lat = $('#latitude').val();
        var long = $('#longitude').val();

        console.log(lat);
        console.log(long);

        var qurl = "/facilities?lat=" + lat + "&long=" + long;

        var query = "/VAFacilitySearch/VASearch" + qurl;

        $.ajax({
            type: "GET",
            dataType: "json",
            url: query,
            success: listFacilities,
            error: failed
        });
    });

    // on success (lol)
    function listFacilities(data) {
        $('.searchResults').empty();

        var tmp = JSON.parse(data);

        $('.searchResults').append('<ul>');
        for (var i = 0; i < tmp.length; i += 1) {
            $('.searchResults').append('<li>"' + tmp[i] + '"</li>');
        }
        $('.searchResults').append('</ul>');
    }
    //on fail (maybe even this far?
    function failed() {
        $('.searchResults').empty();
        $('.searchResults').append('<p>Error Loading Facility Information.</p>');
    }
});