﻿$(function () {
    var chat = $.connection.chatHub;                                         /*Reference to auto-generated proxy for the hub*/
    chat.client.broadcastMessage = function (name, message) {                /*Function that the hub calls back to display message*/
        var encodedName = $('<div />').text(name).html();                    /*Set name*/
        var encodedMsg = $('<div />').text(message).html();                  /*Set message*/
        $('#discussion').append('<li><strong>' + encodedName                 /*Add the message to the page*/
            + '</strong>:&nbsp;&nbsp' + encodedMsg + '</li>');
    };
    $('#displayname').val(prompt('Enter your name: ', ''));                  /*Get the user name*/
    $('#message').focus();                                                   /*Set initial focus to message input box*/
    $.connection.hub.logging = true;
    $.connection.hub.start().done(function () {                              /*Start the connection*/
        $('#sendmessage').click(function () {                                /*Post message when button is pressed*/
            chat.server.send($('#displayname').val(), $('#message').val());  /*Call the send message on the hub*/
            $('#message').val('').focus();                                   /*Clear text box and set the focus for the next comment*/
        });
    });
});