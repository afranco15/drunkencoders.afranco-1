# ProjectVeteranSupport

## Mission Statement (Initial Vision Statement)

**For** veterans across the country **who** are in need of some form of benefit support, **the** current system is complicated at best. This current system **is** layered in bureaucracy **that** slows the entire process downs and can even discourage veterans from even seeking support. Fortunately the United States government and the Department of Veteran's Affairs has recognized this is a problem. Working with several companies, they have created several API's to ease developers in building applications to help veterans around the country. **Unlike** the vast majority of applications out there, **our application** will be one of the few that isn't completely government based, and will have more features than to call for more information.

## Early Needs and Features

1. Clean, simple, and easy to navigate web pages.
2. Ability to create user accounts.
3. Discussion boards for questions.
4. The ability to search the boards for a specific question or topic.
5. Live chat capabilities for those veterans that just need someone to talk to, but don't want to call the crisis line.
6. Find VA facilities near by.
7. The capability to find directions to said VA facilities.
8. Check if any public transportation (bus/train/subway) has stops close by.
9. Ability to schedule or get an Uber or Lyft ride.
10. Estimate the cost of travel for those without a vehicle. 