namespace NewsCheck.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommentRating")]
    public partial class CommentRating
    {
        public int CommentRatingID { get; set; }

        public int CommentsID { get; set; }

        [Required]
        public string UpJustification { get; set; }

        [Required]
        public string DownJustification { get; set; }

        [Required]
        public string FlagJustification { get; set; }

        public virtual Comment Comment { get; set; }
    }
}
