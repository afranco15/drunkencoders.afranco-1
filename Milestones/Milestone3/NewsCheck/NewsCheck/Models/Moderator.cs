namespace NewsCheck.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Moderator")]
    public partial class Moderator
    {
        public int ModeratorID { get; set; }

        public int UsersID { get; set; }

        public virtual User User { get; set; }
    }
}
